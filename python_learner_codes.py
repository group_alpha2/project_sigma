## Casting
# x = str(3)
# y = int(3)
# z = float(3)
# print(x)
# print(y)
# print(z)
# x = 'cub'
# y = "lion"
# z = y,
# print(list(y[:3]))
# #Global variables
# x = 'cute'
# def myfunc():
#     x = 'nice'
#     print('Madrine is '+x)
# myfunc()
# print('Madrine is '+ x)


#Data types
# a = 'Hello World'
# b = 20
# c = 76.7
# d = 1j
# g = ['apple','grapes']
# f = ('apple','grapes')
# x = range(5)
# w = {'apple','grapes'}
# y = {'name': "Eugene", 'age':21}
# v = frozenset({'apple','grapes'})
# q = True
# r = b"Hello"
# z = bytearray(4)
# i = memoryview(bytes(5))
# j = None
# print(type(a))
# print(type(b))
# print(type(c))
# print(type(d))
# print(type(g))
# print(type(f))
# print(type(x))
# print(type(w))
# print(type(y))
# print(type(v))
# print(type(q))
# print(type(r))
# print(type(z))
# print(type(i))
# print(type(j))

#Check if and if Not strings
# txt = 'The best things in life are free!'
# if 'free' in txt:
#     print("yes, 'free' is present")
# else:
#     print("No, it ain't present")
# if 'expensive' not in txt:
#     print("No, 'expensive' aint present")
# else:
#     print("'Expensive' is present")

#Negative Indexing
# b = "Hello World"
# print(b[-5:-2])
# #Changing case
# print(b.upper())
# print(b.lower())
# print(b.title())
# #Replace
# print(b.replace("l","t"))
# #Split string
# print(b.split("l"))
## String format
# x = "Eugene"
# y = "Tuhaise"
# print('Hello, %s. Your other name is %s' %(y,x)) 
# print('Hello, {}. Your other name is {}' .format(x,y)) 
# #Escape characters
# txt = "Hello, my name is \"Eugene Tuhaise\" from MUK."
# print(txt)
#     escap characters include '\', \\, \n, \t, \b, \f, \ooo' (Octal value), \hhh (Hex value)
##Other String methods
# DANTE = '1,2,3'
# print(DANTE)
# this_dict = {"Kampala" : 5, "Mumbai" : 9, "Washington DC" :13, "London" : 24, "Nairobi" :24, "Glasgow" :35}
# print(DANTE.capitalize())
# print(DANTE.casefold())
# print(DANTE.center(2))
# print(DANTE.count('s'))
# print(DANTE.encode())
# print(DANTE.endswith('e'))
# print(DANTE.expandtabs(2))
# print(DANTE.find('b'))
# print(DANTE.format())
# print(DANTE.format_map(3))
# print(DANTE.index('a'))
# print(DANTE.isalnum())
# print(DANTE.isalpha())
# print(DANTE.isascii())
# print(DANTE.isdecimal())
# print(DANTE.isdigit())
# print(DANTE.isidentifier())
# print(DANTE.islower())
# print(DANTE.isnumeric())
# print(DANTE.isprintable())
# print(DANTE.isspace())
# print(DANTE.istitle())
# print(DANTE.isupper())
# print(DANTE.join('a'))
# print(DANTE.rjust(1))
# print(DANTE.partition('a'))
# print(DANTE.replace('a','e'))
# print(DANTE.rfind('g'))
# print(DANTE.rindex('2'))
# print(DANTE.rjust(1))
# print(DANTE.rpartition('a'))
# print(DANTE.rsplit())
# print(DANTE.splitlines())
# print(DANTE.startswith('a'))
# print(DANTE.strip())
# print(DANTE.swapcase())
# print(DANTE.title())
# print(DANTE.translate('1'))
# print(DANTE.zfill(0))

##.maketrans()
# trans_table = str.maketrans("abc", "123")
# original = "abcdef"
# translate = original.translate(trans_table)
# print(translate)
# #Check data type of object
# x = 566
# print(isinstance(x, int))

# #Operators
# x = 8 ,x += 4, x -= 4, x *= 4, x /= 4, x %= 4, x //= 4, x **= 4, x &= 4, x |= 50, x ^= 4, x >>= 4, x <<= 4
# print(int(x))

## LISTS(ordered, changeable, allows duplicate values)
# my_list = ['banana','pen','apple','book','yoghurt','tree','food','James','Cathy']
# other_list = [1,3,5,78]
# sum_list = sum(other_list)
# print(other_list)
# print(sum_list)
# print(my_list)
# #Range of items
# print(my_list[4])
# print(my_list[-4])
# print(my_list[2:6])
# print(my_list[:6])
# print(my_list[3:])
# print(my_list[-6:-1])
# print(len(my_list))
# #Checking if item exists
# if 'pen' in my_list:
#     print('Yes')
# else:
#     print('No')
# my_list[7] = 'group'
# my_list[1:4] = ['hen','frog','car']
# my_list[1:6] = ['hen','frog','car']
# my_list.insert(4, 'table')
# my_list.append('orange')
# my_list.extend(other_list)
# print(my_list)
# #my_list.remove('yoghurt')
# my_list.pop(5)
# my_list.pop()
# del my_list[0]
# #del my_list
# my_list.clear()
# print(my_list)
# #Accessing items
# for x in my_list:
#     print(x)
# for x in range(len(my_list)):
#     print(my_list[x])
# i = 0
# while i < len(my_list):
#     print(my_list[i])
#     i = i + 1
# [print(x) for x in my_list]

##LIST EXAMPLES
# newlist = []
# for x in my_list:
#     if "a" in x:
#         newlist.append(x)
# print(newlist)
# newlist = [x for x in my_list if "a" in x]
# newlist1 = [x for x in my_list if x != 'pen']
# print(newlist)

# newlist = [x.upper() for x in my_list]
# newlist1 = [f'Hello {x}' for x in my_list] 
# newlist1 = [x if x == 'apple' else 'hermit' for x in my_list]
# print(newlist1)

# my_list = ['banana','pen','apple','book','yoghurt','tree','food','James','Cathy']
# numlist = [98,49,95,55,24,11]
# my_list.sort()
# my_list.sort(key= str.lower)
# my_list.reverse()
# numlist.sort()
# numlist.sort(reverse= True)
# print(my_list)
# print(numlist)

# my_list = ['banana','pen','apple','book','yoghurt','tree','food','James','Cathy']
# num_list = [98,49,95,55,24,11,11]
# tot_list = num_list + my_list
# tot_list = my_list + num_list 
# copy_list = list(my_list)
# copy_list = my_list.copy()
# print(copy_list)
# print(tot_list)
# for x in my_list:
#     num_list.append(x)
# print(num_list)
#other list methods
# print(num_list.count(11))

# list = [98,49,95,55,24,11]
# list.sort()
# list.sort(reverse= True)
# print(list)
# #Iterables
# num_list1 = [x for x in range(13)]
# num_list1 = [x for x in range(13) if x >5]
# print(num_list1)



## TUPLES(ordered, unchangeable, allow duplicates)
# my_tuple = ('banana','pen','apple','book','yoghurt','tree','food','James','Cathy')
# print(len(my_tuple))
# print(type(my_tuple))
## Accessing tuple items, ranges of items, checking if item exists, same as in lists
## Change tuple values
# x = list(my_tuple)
# x[6] = 'guard'
# x.append('football')
# print(tuple(x))
# y = ('basket',)
# my_tuple += y
# print(my_tuple)
# x.remove('banana')
# print(tuple(x))

##Unpacking a list
# new_tuple = ('earth','air','water','fire')
# (k, *l, m) = my_tuple
# (a, b, c, d) = new_tuple
# print(a)
# print(b)
# print(c)
# print(l)

##Loop tuples
# for x in new_tuple:
#     print(x)
# for i in range(len(new_tuple)):
#     print(new_tuple[i])
# j = 0
# while j < len(new_tuple):
#     print(new_tuple[j])
#     j += 1

##Join tuples
# tuple_join = my_tuple + new_tuple
# double_tuple = new_tuple*5  #(Multiply tuples)
# print(tuple_join)
# print(double_tuple)

# #tuple_try out
# n = int(input("Enter the number of items desired: \n"))
# items = []
# for i in range(n):
#     number = input("Enter an item: \n")
#     items.append(number)
# items = tuple(items)
# print(f"Your Tuple is: \n{items}")

# student_data = ((30,20,60,80,65), (20,10,46,1,34), (93,34,58,58,66), (59,58,58,78,15))
# (student_1,student_2,student_3,student_4) = student_data
# for i in range(4):
#     print(f"Total for Student {i + 1}: {sum(student_data[i])}")


##SETS(unordered, unchangeable, unindexed, duplicates not allowed)
# my_set = {'banana','pen','apple','book',True,'tree',9,'James','Cathy'}
# new_set = {12,34,57,87,45}
# dupli_set = {'lion','pen','Cathy','eagle',True,'joy'}
# print(my_set)
# print(len(my_set))
# print(type(my_set))
# print('Cathy' in my_set)

##(Accessing set items, ranges of items, checking if item exists, same as in lists & tuples)
##(You can't change set items, but u can add new items)
# my_set.add('boy')
# print(my_set)
# my_set.update(new_set)
# set_join = my_set.union(new_set)
# print(set_join) #(You can not only add a set, but tuples, lists, dictionaries)
# my_set.remove('tree')
# my_set.discard('tree')
# x= my_set.pop()  #(Cannot take any arguments)
# my_set.clear()  #del my_set

# print(x)
# print(my_set)
##Loop sets
# for i in my_set:
#     print(i)
##Keep only duplicates
# my_set.intersection_update(dupli_set)
# x = my_set.intersection(dupli_set)
# print(my_set)
# print(x)
## Keep all, but not the duplicates
# my_set.symmetric_difference_update(dupli_set)
# x = my_set.symmetric_difference(dupli_set)
# my_set.discard("pen")
# print(my_set)
# print(x)
##Other set methods
# copy() difference() discard() isdisjoint() issubset() 


## DICTIONARIES(ordered, changeable, duplicates not allowed)
# this_dict = {"brand": "Ford", 'electric': False, "model": "Mustang", "year": 1965,"year": 2020, 'colors': ['red', 'white', 'black']}
# my_dict = dict(brand= "Ford", electric = False, model= "Mustang", colors= ['red','black'], year= 1965)
# print(this_dict)
# print(my_dict)
# print(len(this_dict))
# print(type(this_dict))
## Access items
# print(this_dict['model'])
# print(my_dict['colors'])
# x = this_dict.get('model')
# print(this_dict.get('model'))
# print(this_dict.keys())
# print(this_dict.values())
# print(this_dict.items())
# my_dict['color'] = 'black'  #Adding items
# this_dict['color'] = 'gold'  #Adding items
# print(x)
# print(my_dict)
# print(this_dict)
##Check if key exists
# if 'year' in this_dict:
#     print('Yes')
# else:
#     print('No')
##Changing dict items
# my_dict.update({'year' : 2020})
# this_dict.pop('electric')
# del this_dict['model']
# #del this_dict
# my_dict.popitem()
# print(my_dict)
# print(this_dict)
# my_dict.clear()
# print(my_dict)
##Loop dictionaries
# for x in my_dict:
#     print(x)
#     print(my_dict[x])
# for i in this_dict.values():
#     print(i)
# for x in this_dict.keys():
#     print(x)
# for x, y in this_dict.items():
#     print(f'{x} : {y}')
##Nested Dictionaries
# students = {'1' :{'name': 'Emil', 'year': 1918}, '2' : {'name': 'Peter', 'year': 1956}, '3' : {'name': 'Jane', 'year': 3030}}
# for x,y in students.items():
#     print(f'Student:{x}')
#     for i,j in y.items():
#         print(f'{i.capitalize()} : {j}')
##Access items in nested dictionaries
# print(students['3']['name'])
##Other methods
# copy() fromkeys() get() setdefault() 

##dictionaries #Finding sum and average
# this_dict_temp = {"Kampala" : 28, "Mumbai" : 30, "Washington DC" :13, "London" : 24, "Nairobi" :28, "Glasgow" :15}
# total_sum = 0
# count = 0
# for value in this_dict_temp.values():
#     total_sum += value
#     count+=1
# average = total_sum/count
# print(f"The Sum of Figures: \n{total_sum}")
# print("The Average temperature is \n%.1f "  %average)

#For Loops
# marks = [10,30,29,80,56,90,22,83]
# for i in marks:
#     if i == marks[7]:
#         print(i)
#     else:
#         print(i, end=':')

# for x in range(1,11,3):
#     print(x)
# marks = [10,30,29,80,56,90,22,83]
# above_80 = False
# for grade in marks:
#     print('checking',grade,',')
#     if grade > 80:
#         above_80 = True
#         break
# print('Therefore, Above 80 exists:',above_80)

# marks = [20,30,1,12,5,15,6,78,10]
# above_80 = False
# print('Below 20:') 
# for grade in marks:
#     if grade >= 20:
#         continue
#     print(grade)

# maximum = int(input("Please enter maximum value: \n"))
# total = 0
# for num in range(1, maximum +1):
#     if (num%2 ==0):
#         total = total + num
#         print(total)
# print('Sum of Even numbers from 1 to {1} = {0}'.format(total,num))

#Nested For Loops
# rows = 5
# columns = 6
# for i in range(rows):
#     for j in range(columns):
#             print(i+j+1,end='   ')
#     print('\n')

# for i in range(5):
#     for j in range(i+1,i+7):
#         if j ==i+6:
#             print(j)
#         else:
#             print(j,end='  ')

# for i in range(5):
#     print('\n')
#     for j in range(6):
#         print(i+j+1, end ='  ')

# x = 3
# for i in range(x):
#     print('------',i)
#     for j in range(x):
#         print(j)

#If-else statements
# age = int(input('Enter ur age: \n'))
# sex = (input('Enter ur sex: \n'))
# if age >= 18:
#     if sex.upper() == 'M': print('Hello Sir')
#     elif sex.upper() == 'F': print('Hello Madam')
# elif age < 18:
#     if sex.upper() == 'M': print('Hello Boy')
#     elif sex.upper() == 'F': print('Hello Girl')

# amt = int(input('Enter Sale amount: \n'))
# if amt >0:
#     if amt<=5000:
#         disc = amt*0.05
#     elif amt<=15000:
#         disc = amt*.12
#     elif amt<= 25000:
#         disc = amt*0.2
#     else:
#         disc = amt *0.3

#     print('Discount: ', int(disc))
#     print('Net Pay: ', int(amt-disc))
# else:
#     print('Invalid amount')

#pass statements
# a = 7
# b = 8
# if a > b:
#     pass
# print(a<<b)

#Range() function
# top = int(input('Enter the max. number:\n'))
# for i in range(0,top,10):
#     print(i)
# x = [24,35,22,44,77]
# for i in x:
#     print(f"The figure is {i} and the number is {i} ")

# user_name = input("Enter your name: \n")
# x = int(input("Number of times: \n"))
# for i in range(x):
#     print(user_name)

# data = [[13,45,22,7],[35,58,39,77],[46,59,93,97],[37,25,36,75]]
# extract = ''
# for i in range(len(data)):
#     for j in range(len(data)):
#         if i==j:
#             extract +=str(data[i][j])+','
# print(extract[:-1])

# heights = {
#     'Plant1': {'Frequency': 4, 'Height of plants': 2.1},
#     'Plant2': {'Frequency': 2, 'Height of plants': 2.4},
#     'Plant3': {'Frequency': 1, 'Height of plants': 2.5},
#     'Plant4': {'Frequency': 4, 'Height of plants': 2.7},
#     'Plant5': {'Frequency': 5, 'Height of plants': 3.0},
#     'Plant6': {'Frequency': 6, 'Height of plants': 3.1},
#     'Plant7': {'Frequency': 2, 'Height of plants': 3.5}
#     }
# for i,j in heights.items():
#     print(f'{i}')
#     for x,y in j.items():
#         print(f'{x} : {y}')
#     for freq, hgt in j.items():
#         if freq =='Frequency':
#             print(f'Frequency: {hgt}')

# even_count = 0
# odd_count = 0
# numbers = '8,3,10,16,13,12,8,8,3,4'
# numbers = numbers.split(',')
# numbers = [int(num) for num in numbers]
# # print(numbers)
# for num in numbers:
#     if num > 0:
#         if num % 2 == 0:
#             even_count += 1
#         else:
#             odd_count +=1
# print(f'Count of even numbers: {even_count}')
# print(f'Count of odd numbers: {odd_count}')

# tot_mark = 0
# sub_num = 4
# for i in range(sub_num):
#     course_work = int(input(f'Enter the marks for course unit {i+1}: '))
#     tot_mark += course_work
# Average = tot_mark/sub_num
# print(int(Average))
# if Average >=90:
#         print('A+')
# elif Average >=80:
#         print('A')
# elif Average >=70:
#         print('B')
# elif Average >=60:
#         print('C')
# else:
#         print('F')


# numbers = "8,3,10,16,13,12,8,8,3,4"
# numbers = numbers.split(',')
# numbers = [int(num) for num in numbers]
# number_count = {}
# print((numbers))
# # print(max(numbers))
# for i in numbers:
#     if i in number_count:
#         number_count[i] += 1
#     else:
#         number_count[i] = 1
# frequent_number = max(number_count, key=number_count.get)
# print(f'The most frequent number is {frequent_number} with a frequency of {number_count[frequent_number]} times.')

# text = 'EPIC'
# print(f'{text:.^10}')

## While loops
# i = 2
# while i<10:
#     print(i)
#     i +=1
# i = 0
# while i<10:
#     i += 1
#     print(i, end='')
# i = 0
# while i < 6:
#     i += 1
#     if i == 3:
#         continue
#     print(i)

# num = [4,5,7,8,1,3,6,9]
# sum =0
# counter = 0
# while counter < len(num):
#     if num[counter]%2 ==0:
#         sum += num[counter]
#     counter+=1
# print('Sum of even is: ', sum)

# s1 = 'This is very long. It is a sentence'
# s2 = 'is'
# print(s1.count(s2)) #Using a .count function

# digit = list(input('Enter any number: \n'))
# digit = [int(num) for num in digit]
# sum,counter = 0,0
# while counter < len(digit):
#     sum += digit[counter]
#     counter+=1
# print(sum)

# m = int(input('Enter the starting value:\n'))
# n = int(input('Enter the ending value:\n'))
# x = [i for i in range(m+1,n)]
# sum,count= 0,0
# while count < len(x):
#     if x[count]%2 == 0:
#         sum += x[count]
#     count +=1
# print('The Sum of Even values in this range is: \n',sum)

# score = float(input('Enter a score between 0.0 and 1.0:\n'))
# if score <= 1.0 and score >= 0.0:
#     if score >= 0.9:
#         print('A')
#     elif score >= 0.8:
#         print('B')
#     elif score >= 0.7 :
#         print('C')
#     elif score >= 0.6 :
#         print('D')
#     else :
#         print('F')
# else:
#     print('Error!!. Please Try Again')

##Functions
# def functionName(parameters):
#     variable = expression(parameters)
#     return variable
# functionName()
# def my_func(x):
#     print('Hello ',x)
# my_func('John')
# def var(*y):
#     print('The youngest one is', y[0])
# var('Joe','Tom','King')
# def var(x,y,z='Jovia'):
#     print('My name is %s' %x)
#     print('My name is '+ y)
#     print('My name is '+ z)
# var(x = 'Joe',y = 'Tom',z = 'King')
# var('Joe','Tom','King')
# def var(**alp):
#     print('My name is '+ alp['x'])
# var(x = 'Tom', y = 'Joe',z = "Mahad")
# def var(x = 'James'):
#     print('My name is '+ x)
# var('Joe')
# var()
# var('Peter')

# def var(x):
#     for i in x:
#         print(i)
# my_list = ['a','b','c']
# var(my_list)

# def var(x,y):
#     return x*y
# print(var(6,7))
# x = var(5,14)
# print(x)

#7*fac(6)   6*fac(5)    

# def var(k):
#         if(k>0):
#               result = k + var(k-1)
#               print(result, end=' ')
#         else:
#               result = 0
#         return result
# print('\nRecursion Example Results')
# var(4)

# def fac(n):
#        if n == 1:
#             return 1
#        else:
#             return  n*fac(n-1)

# x = fac(7)
# print(x)

# def var(x,y,z):
#     print('Mr. %s, Dr. %s, and the %s are present' %(x,y,z))
# var('Joe','Tom','King')

# def f(x):
#     print(x[0])
#     x[0]='grape'
# fruits = ['corn','yam','lime']
# f(fruits)
# print(fruits[0])


# def sum(x,y,z=0,w=0):
#     return x+y+z+w
# user_input = ""
# while user_input.lower() != 'done':
#     x,y,z,w = 0,0,0,0
#     n = int(input('How many numbers do u want to sum?\n'))
#     for i in range(1,n+1):
#         if i == 1:
#             x = input('Enter number %s\n' %i)
#         elif i==2:
#             y = input('Enter number %s\n' %i)
#         elif i==3:
#             z = input('Enter number %s\n' %i)
#         else:
#             w = input('Enter number %s\n' %i)
#     # x = input('Enter number 1')
#     # y = input('Enter number 2')
#     # z = input('Enter number 3')
#     # w = input('Enter number 4')
#     total = sum(int(x),int(y),int(z),int(w))
#     print('Total:\n',total)
#     user_input = input('Enter done to exit.\n')


# def sum():
#     total = 0
#     while True:
#         n = (input('Enter the number :\n'))
#         if n.lower() == 'done':
#             break
#         try:
#             n = int(n)
#             total += n
#         except ValueError:
#             print('Invalid input')
#     return total
# result = sum()
# print(result)

# def sum(n):
#     total = 0
#     for i in range(n):
#           while True:
#             x = input(f'Enter the value {i+1}:\n')
#             if x.isdigit():
#                 x = float(x)
#                 total += x
#                 break
#             else:
#                 print('Invalid input!!!')
#     return total
# # num = int(input('Enter the number of values to be summed:\n'))
# # result = sum(num)
# # print('Total: \n',int(result))
# print(sum(5))

# def fac(n):
    # if n == 1:
    #     return n
    # else:
    #     return print(fac(n-1)*n)
# x = int(input('Enter a number:\n'))
# print('',fac(x))

# import test as x
# print(x.fac(10))

# x = dir(test)
# print(x)


# def myfunc(x):
#     return abs(x - 100)

# def myfunc():
#     global x
#     x = 'fantastic'
# myfunc()
# print('Bombastic '+ x)

# def f():
#     global y
#     y = 5
# y = 1
# print(y)

# def func(x):
#     for i in range(1,x+1):
#         print('{}\t{}'.format(i,i**2))
# func(3)

# def add_shipping (subtotal):
#     subtotal = subtotal/5
#     return subtotal
# units = int(input('Enter number of units:\n'))
# firstTotal = units * 15
# total = add_shipping(firstTotal)
# print('Your total is ',int(total))

## Lambda expressions

# def f(a,b):
#       return a+b

# x = lambda a,b: a+b
# num1 = int(input('Enter a number:\n'))
# num2 = int(input('Enter another number:\n'))
# print('The total is:\n ',x(num1,num2))

# def myfunc(a):
#     return lambda n: a*n
# doubler = myfunc(2)
# tripler = myfunc(3)
# print(doubler(100))
# print(tripler(100))


# def f(x):
#     return x+x
# z = 3
# b = f(z)
# y = [4]
# a = f(y)
# print(b)
# print(a)


# def sum():
#     total = 0
#     while True:
#         n = (input('Enter the number :\n'))
#         if n.lower() == 'done':
#             break
#         n = int(n)
#         total += n

#     return total
# result = sum()
# print(result)

# #Exception handling
# x = -1
# if x<0:
#     raise Exception("Sorry!!!")

# def divide(a,b):
#     if b == 0:
#         raise ValueError("Dont be stupid!!!!! You cannot divide by zero")
#     return  a/b
# # print(divide(10,0))
# try:
#     result = divide(10,2)
#     print(int(result))
# except ValueError as x:
#     print(f"An error occured: {x}")

# sum1 = 0
# def avg(n):
#     avg = n/5
#     return avg


# list1 = [2.0,2.0,2.0,2.0,2.0]
# for i in list1:
#         sum1 += i
# # x = avg(sum1)
# # print(x)
# print(avg(60))

# import math
# # x = dir(math)
# # print(x)
# print(math.sin(30))


# #CLASSES
# class MyClass:
#     x = 76
# y = MyClass()
# print(y.x)
# print(MyClass().x)

# class servant:
#     def __init__(self,x,y):
#         self.firstname = x
#         self.lastname = y
#     def displayName(fx):
#         print("Hello, my name is ",fx.firstname, fx.lastname)
# x = servant("Gene","Tuhaise")
# # x.displayName()
# # print(x)
# print(x.firstname)

# class machines:
#     def __init__(x, name = "Benz", brand= "E-class"):
#         x.name = name
#         x.brand = brand
#     def cars(n):
#         print(f"My {n.name} is driving!")
#     def boats(n):
#         print(f"My {n.name} is sailing!")
#     def plane(n):
#         print(f"My {n.name} is flying!!")
# x = machines()
# w = machines("Toyota","Corona")
# y = machines("Yacht","GenZ")
# z = machines("Boeing","747")
# x.cars()

# class servant:
#     def __init__(self,name,age):
#         self.name = name
#         self.age = age
#     def displayServant(self):
#         print(f"Name: {self.name}, age :{self.age}")
# print("servant.__doc__:", servant.__doc__)
# print("servant.__name__:", servant.__name__)
# print("servant.__module__:", servant.__module__)
# print("servant.__bases__:", servant.__bases__)
# print("servant.__dict__:", servant.__dict__)


# class servant:
#     servCount = 0
#     def __init__(self,name,age):
#         self.name = name
#         self.age = age
#         servant.servCount += 1
#     def disp_servant(self):
#         print("Name: ", self.name, "\nAge: ", self.age)#, "\nEmp. ID: ", servant.servCount)
#         # return("Emp. ID: ", servant.servCount)
# e1 = servant("John",45)
# e2 = servant("Peter",55)
# e3 = servant("James",41)
# e4 = servant("Jacob",25)
# e1.disp_servant()
# setattr(e1,"salary",7000)
# # x.salary = 7000
# print(getattr(e1,"salary"))


# class servant:
#     servCount = 0
#     def __init__(self,name,age):
#         self.name = name
#         self.age = age
#         servant.servCount += 1
#     # @classmethod
#     def showcount(self):
#         print(self.servCount)
#     count = classmethod(showcount)
# e1 = servant("John", 29)
# e2 = servant("Peter", 35)
# e3 = servant("James", 25)
# e4 = servant("Paul", 59)
# e4.showcount()
# servant.count()



# class servant:
#     servCount = 0
#     def __init__(self,name,age):
#         self.name = name
#         self.age = age
#         servant.servCount += 1
#     @classmethod
#     def showcount(self):
#         print(self.servCount)
#     # count = classmethod(showcount)

# e1 = servant("John", 29)
# e2 = servant("Peter", 35)
# e3 = servant("James", 25)
# e4 = servant("Paul", 59)
# servant.showcount()


# class servant:
#     servCount = 0
#     def __init__(self,name,age):
#         self.name = name
#         self.age = age
#         servant.servCount += 1
#     def disp_servant(self):
#         print("Name:", self.name, "\nAge:", self.age, "\nEmp. ID:", servant.servCount)
#     @staticmethod
#     def showcount():
#         print(servant.servCount)
#     #     return
# #    # count = staticmethod(showcount)
# # n = int(input("Enter the number of employee:\n"))
# # for i in range(n):
# #     for j in range(n):
# #         e = input("Enter the employee name:\n")
# #         a = int(input("Enter the employee age:\n"))
# #         emp = servant(e,a)
# #     emp.disp_servant()
# e1 = servant("John", 29)
# e2 = servant("Peter", 35)
# e3 = servant("James", 25)
# e4 = servant("Paul", 59)
# e4.showcount()
# servant.showcount()

## FILE HANDLING
# from colorama import init, Fore, Style, Back
# init()
# def style_text(text):
#     print(f'{Back.YELLOW}{Fore.LIGHTGREEN_EX}{text}{Style.RESET_ALL}')
# my_doc = Document('practice_python.docx')
# for i in my_doc.paragraphs:
#     style_text(i.text)


# with open('DOCOCK.txt','w') as file:
#     file.write('This is a plain text document with extension .txt created from python')
# file.close()          ## no need for a .+close()

# with open('test text.txt','r') as file:
#     cont = file.read()
# word_content = f"{cont}"
# with open('test.docx','t+w') as word_file:
#     word_file.write(word_content)

# from docx import Document
# x = Document()
# x.add_heading('MAHAD',0)
# x.add_paragraph("Mugisha Balaam\nTuhaise Eugene\nMark Myles")
# x.add_paragraph('Tuhaise Eugene')
# x.add_paragraph('Mark Myles')
# x.save('python-to-MSWord.docx')
# x.add_heading("Names",0)
# x.add_paragraph("Laura\nJovia\nEugene\nMark")
# x.save("MAHAD.docx")

# def modify_doc(input,output,new_data):
#     with open(input,'rb') as file:
#         doc = Document(file)
#         doc.add_paragraph(new_data)
#         doc.save(output)
# if __name__ == '__main__':
#     input_document = 'MAHAD.docx'
#     output_document = 'MAHAD_mod.docx'
#     add_data = 'My new data'
#     modify_doc(input_document,output_document,add_data)
# my_doc = Document('MAHAD.docx')
# for i in my_doc.paragraphs:
#     print(i.text)
# with open('madrine.txt','r') as x:
#     cont = x.read()
# word_doc = cont
# with open('MADRINE.docx','') as y:
#     y.write(word_doc)

# from docx import Document
# x = Document("MADRINE.docx")
# for i in x.paragraphs:
#     print(i.text)
# x.add_heading("MADRINE'S LIFE",0)
# x.add_paragraph("Madrine\nEugene")
# x.save("MADRINE.docx")

## CLASS INHERITANCE
# class division:
#     '''# def __init__(x,a,b):
#     #     x.n = a
#     #     x.d = b'''
#     def divide(x,a,b):
#         return a/b
# '''    def divide(x):
#         return x.n/x.d'''
# class modulus:
#     '''# def __init__(x,a,b):
#     #     x.n = a
#     #     x.d = b'''
#     def mod_divide(x,a,b):
#         return (a%b)
# '''    def mod_divide(x):
#         return (x.n%x.d)'''
# class Div_Mod(division,modulus):
#     pass
#     '''# def __init__(x,a,b):
#     #     x.n = a
#     #     x.d = b
#     # def div_mod(x):
#     #     divVal = division.divide(x)
#     #     modVal = modulus.mod_divide(x)
#     #     return (divVal, modVal)'''
# num = int(input("Enter the numerator:\n"))
# den = int(input("Enter the denominator:\n"))
# x = Div_Mod()
# y = x.divide(num,den)
# z = x.mod_divide(num,den)
# print("DIVIDE: ", y)
# print("MODULUS: ", z)
# x = Div_Mod(num,den)
# print("REsult:")
# print("DIVISION: ", x.divide())
# print("MODULUS: ", x.mod_divide())
# print("OVERALL: ", x.div_mod())

## POLYMORPHISM
# class car:
#     def __init__(fx, x,y):
#         fx.brand = x
#         fx.model = y
#     def move(fx):
#         print("Drive!")
# class boat:
#     def __init__(fx, x,y):
#         fx.brand = x
#         fx.model = y
#     def move(fx):
#         print("Sail!")
# class plane:
#     def __init__(fx, x,y):
#         fx.brand = x
#         fx.model = y
#     def move(fx):
#         print("Fly!!")

# car1 = car("Toyota","Harrier")
# boat1 = boat("Benz","E-class")
# plane1 = plane("Boeing","747")
# plane1.move()
# for x in (car1,boat1,plane1):
#     x.move()

# class laura:
#     pass

# x = 'laura'
# try:
#     print(x)
# except NameError:
#     print("invalid!!!!!!!")
# else:
#     print("  ")

# def write_to_word(file,content):
#     with open(file,'a') as fx:
#         fx.write(content)
# if __name__ == '__main__':
#     word_document = 'OUTPUT_DOCUMENT_GROUP12.doc'
# #     output_document = 'MAHAD_mod.docx'
#     add_data = 'My new data, shabashabadooooo!!!!!!!!!!!!!!!'
#     write_to_word(word_document,add_data)

# using map function
# def square(x):
#     return x**2
# num = [3.5,2.4,55.6,3]
# # for i in num:
# #     print(square(i))
# square_num = map(square,num)
# square_num = tuple(square_num)
# print(square_num)

## zip function
# names = ['Angel','Banana','Canada']
# values = [1,3,5]
# comb = zip(names,values)
# comb = dict(comb)
# print(comb)

# x = lambda k: k*5
# print(x(45))

# def myagecalc(m):
#     return lambda n: n-m
# age = myagecalc(2003)
# print(age(2023))


# def myfunc(a):
#     return lambda n: a*n
# doubler = myfunc(2)
# tripler = myfunc(3)
# print(doubler(100))
# print(tripler(100))

# # select = []
# # fruits = list(fruits)
# fruits_add=('guava','passion')
# print(fruits+fruits_add)
# # for i in fruits:
#     if 'an' in i:
#         select += i
#         print(select)
# for i in range(len(fruits)):
#     if i>4:
#         break
#     if i%2 == 1:
#         print(fruits[-i])

# mark_list = []
# for i in range(3):
#     user = int(input(f'Enter test score {i+1}: '))
#     mark_list.append(user)
# print(mark_list)
# for i in mark_list:
#     if i > 100:
#         print('Value over 100!!!!')
# for i in mark_list:
#     sum += i
# avg = sum/len(mark_list)
# print(int(avg))

# maxm = int(input('Enter the maximum value: '))
# sum = 0
# for i in range(1,maxm+1):
#     if i%2 == 0:
#         sum += i
#         print (sum)
# print(f'Sum of even numbers from 1 to {i} = {sum}')

# class Student:
#     next_id_num = 1
#     def __init__(self,reg_number,name):
#         self.reg_number = reg_number
#         self.name = name
#         self.id_number = Student.next_id_num
#         self.__id = Student.next_id_num
#         Student.next_id_num += 1
#     def print_stud_details(self):
#         print('Name: %s\nReg No: %s\nId: %s\nTotal Students: %s'%(self.name,self.reg_number,self.id_number,Student.next_id_num-1))
# class CS_Student(Student):
#     course = 'Computer Science'
#     def __init__(self, reg_number, name):
#         Student.__init__(self,reg_number, name)
#     def print_stud_details(self):
#         Student.print_stud_details(self)
#         print('Course:',CS_Student.course)


# student1 = Student('2016/U/001','Gene Haise')
# student2 = CS_Student('2016/U/002','Namavu Genna')
# student3 = CS_Student('2016/U/003','Girama Felix')

# student1.print_stud_details()
# student2.print_stud_details()
# student3.print_stud_details()

# def fun(x):
#     for i in range(x-1,x*x):
#         print('{}\t{}'.format(i,i*i))
# fun(3)

# x = input('Enter a number: ')
# y = int(input('Enter another number: '))
# z = y/x
# print(z)

# task_b = 'b'
# for i in range(1000):
#     print(task_b)

# user = input('Enter your name: ')
# n = int(input('Enter number of times: '))
# for i in range(n):
#     print(user)
# def f(x):
#     global y
#     y = 90
#     x = y+x
#     return x
# x = 3
# y = 4
# y = 9
# z = f(x)
# print('x is',x)
# print('y is',y)
# print('z is',z)

# class Staff:
#     def __init__(self,name,date_of_birth):
#         self.name = name
#         self.DOB = date_of_birth
#     def print_staff_details(self):
#         print('Name: {1}\nDate Of Birth: {0}'.format(self.DOB,self.name))
# class Secretary(Staff):
#     pass
# class Driver(Staff):
#     def __init__(self, name, date_of_birth,driving_license_no):
#         Staff.__init__(self,name, date_of_birth)
#         self.drivers_license_no = driving_license_no
#     def print_staff_details(self):
#         Staff.print_staff_details(self)
#         print('License No: {}'.format(self.drivers_license_no))

# staff1 = Staff('John',1987)
# staff2 = Secretary('Mary',1978)
# staff3 = Driver('Bob',1979,'05/XP/1325')

# staff1.print_staff_details()
# staff2.print_staff_details()
# staff3.print_staff_details()

# x = [[1,2,3,4],[5,6,7,8],[9,10,11,12],[13,14,15,16]]
# extract = ''
# for i in range(len(x)):
#     for j in range(len(x)):
#         if i == j:
#             extract += str(x[i][j])+','
# print(extract[:-1])

# marks = {'2201':30,'2202':80,'2203':45,'2204':27,'2205':31,'2206':31,'2207':32,'2208':90,'2209':45,'2210':45}
# # best_student = max(marks,key=marks.get)
# # print(best_student)
# # print(len(marks))
# # sum = 0
# # for i in marks.values():
# #     sum += i
# # avg = sum/len(marks)
# # print('Mean marks is: ',avg)

# count = {}
# for i in marks.values():
#     if i in count:
#         count[i]+=1
#     else:
#         count[i]=1
# print(count)
# mode = max(count,key=count.get)
# print('The mode of the class is',mode)
# print('The mode frequency is',count[mode])

# x = ['cat','dog','rat']
# x1 = x
# x2 = x.copy()
# x1.append('pig')
# # print(x)
# # print(x1)
# # print(x2)
# my_set = set(x2)
# my_set.discard('dog')
# my_set.add('ape')
# print(my_set)
# fruits = ('apple','banana','jackfruit','orange','kiwi','melon','mango')
# for i in fruits:
#     if 'an' in i:
#         print(i)
#         break
# x = ('guava','passion')
# print(fruits+x)





# heights = {
#     'Plant1': {'Frequency': 4, 'Height of plants': 2.1},
#     'Plant2': {'Frequency': 2, 'Height of plants': 2.4},
#     'Plant3': {'Frequency': 1, 'Height of plants': 2.5},
#     'Plant4': {'Frequency': 4, 'Height of plants': 2.7},
#     'Plant5': {'Frequency': 5, 'Height of plants': 3.0},
#     'Plant6': {'Frequency': 6, 'Height of plants': 3.1},
#     'Plant7': {'Frequency': 2, 'Height of plants': 3.5}
#     }
# print(heights)
# for i,j in heights.items():
#     print(i,j)
    # for x,y in j.items():
    #     if x == 'Frequency':
    #         print('Frequency:',y)

# for i in range(5):
#     for j in range(i+1,i+5):
#         if j == i+4   :
#             print(j)
#         else:
#             print(j,end=" ")

# class MyClass:
#     def __init__(self):
#         self.__priv_variable = 24
#     def get_priv_variable(self):
#         return self.__priv_variable
#     def set_priv_variable(self,value):
#         if value>0:
#             self.__priv_variable = value
# obj = MyClass()
# # obj.set_priv_variable(99)
# print(obj.get_priv_variable())

# class BankAccount:
#     def __init__(self,account_holder,balance):
#         self._account_holder = account_holder
#         self._balance = balance
#     def deposit(self,amount):
#         if amount>0:
#             self._balance += amount
#     def withdraw(self,amount):
#         if 0<amount<= self._balance:
#             self._balance-= amount
#         else:
#             print('Insufficient Funds!!')
#     def get_balance(self):
#         return self._balance
#     def get_account_holder(self):
#         return self._account_holder


# x = 50
# y = 40
# z = 20
# if x < y:
#     print('x is younger than y')
#     if z<x:
#         print('z is younger than x')
#         if z>y:
#             print('z is older than y')
# else:
#     print('y is younger than x')

# n = 10
# score_list = [12, 33, 43, 55, 44, 33, 22, 65, 98, 77]
# for i in range(n):
#     user = int(input(f'Enter test score {i+1}: '))
#     score_list.append(user)
# print(score_list)
# print(max(score_list))
# print(min(score_list))
# sum = 0
# for i in score_list:
#     sum += i
# avg = sum/len(score_list)
# print(avg)
# sorted_score = sorted(score_list,reverse=True)
# print(sorted_score)
# print('The second Largest is: ',sorted_score[1])